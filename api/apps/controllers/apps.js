const { parseMultipartData, sanitizeEntity } = require('strapi-utils');

module.exports = {
    async create(ctx) {
        let entity;
        const defaultPackage = await strapi.query('plans').find({ price: 0 });
        console.log(defaultPackage[0]._id, ctx.request.body)
        if (ctx.is('multipart')) {
            const { data, files } = parseMultipartData(ctx);
            entity = await strapi.services.apps.create(data, { files });
        } else {
            ctx.request.body['planID'] = defaultPackage[0]._id
            entity = await strapi.services.apps.create(ctx.request.body);
            const entityy = await strapi.services.subscription.create({ appID: entity._id, planID: defaultPackage[0]._id, active: true });
        }
        return sanitizeEntity(entity, { model: strapi.models.apps });
    },
    async update(ctx) {
        const { id } = ctx.params;
        if (ctx.request.body.planID) {
            console.log(ctx.request.body)
            const entry = await strapi.query('subscription').update({ appID: id, active: true }, { active: false });
            const entityy = await strapi.services.subscription.create({ appID: id, planID: ctx.request.body.planID, active: true });
        }
        let entity;
        if (ctx.is('multipart')) {
            const { data, files } = parseMultipartData(ctx);
            entity = await strapi.services.apps.update({ id }, data, {
                files,
            });
        } else {
            entity = await strapi.services.apps.update({ id }, ctx.request.body);
        }

        return sanitizeEntity(entity, { model: strapi.models.apps });
    },
};
