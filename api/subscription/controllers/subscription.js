'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
    async update(params) {
        strapi.services.subscription.update(params)
        return (strapi.model.subscription);
    }
};
