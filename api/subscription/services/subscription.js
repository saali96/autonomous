'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */

module.exports = {
    async update(params) {
        const existingEntry = await strapi.query('subscription').find({ $and: [{ appID: params }, { active: true }] });

        const validData = await strapi.entityValidator.validateEntityUpdate(
            strapi.models.subscription,
            { active: false },
            { isDraft: isDraft(existingEntry, strapi.models.subscription) }
        );
        const entry = await strapi.query('restaurant').update(params, validData);
        return entry;
    }
};
